{-# LANGUAGE PackageImports #-}
import "interview-example" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
