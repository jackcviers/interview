{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Handler.PersonSpec (spec) where

import TestImport
import Data.Aeson


spec :: Spec
spec = withApp $ do

  describe "Person" $ do
    describe "when initialized"  $ do
      it "should be empty" $ do
        get PersonR
        statusIs 200
        bodyContains "[]"
          
