module Handler.Person where

import Import
import Data.Aeson.Types

postPersonR :: Handler Value
postPersonR = do
  person <- (requireJsonBody :: Handler Person)
  insertedPerson <- runDB $ insertEntity person
  returnJson insertedPerson

getPersonR :: Handler Value
getPersonR = do
  people <- runDB $ getAllPeople
  returnJson people

deletePersonR :: Handler Value
deletePersonR = do
  people <- runDB $ deleteWhere ([] :: [Filter Person])
  sendResponseStatus status204 ("DELETED")
  


getAllPeople :: DB [Entity Person]
getAllPeople = selectList [] [Asc PersonId]
